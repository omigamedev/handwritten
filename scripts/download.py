import urllib.request
import os.path

for hand in ["left", "right"]:
    for c in range(ord('A'), ord('Z')+1):
        name = chr(c)+".json"
        url = "http://theuniversaltypeface.com/data/explore/handedness/"+hand+"/"+name
        file = "../data/"+hand+"/"+name
        if not os.path.exists(file):
            print(url)
            urllib.request.urlretrieve(url, file)