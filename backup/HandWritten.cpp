#include "stdafx.h"
#include "RenderTexture.h"
#include "Sample.h"

namespace ml = cv::ml;

const char FIRST = 'A';
const char LAST = 'Z';
const int W = 16;
const int H = 16;
//const int D = 10; // Number of components to keep for the PCA

const bool IGNORE_CACHE = true;
GLFWwindow* window;

void initOpenGL()
{
    printf("init\n");
    glfwInit();
    glfwWindowHint(GLFW_VISIBLE, false);
    window = glfwCreateWindow(800, 600, "Reader", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, [](GLFWwindow* w, int key, int, int, int)
    {
        if (key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(w, 1);
    });

    glewInit();
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_DST_COLOR, GL_ZERO);
    glPointSize(15);
    glLineWidth(1);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1, 0, 1, -1, 1);
    glMatrixMode(GL_MODELVIEW);
}

void UnloadOpenGL()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

std::vector<Sample> ReadSamples(const char* filename)
{
    std::vector<Sample> ret;
    rj::Document doc;
    rj::FileStream fs(std::fopen(filename, "r"));
    doc.ParseStream(fs);
    for (auto& item : doc)
    {
        ret.emplace_back();
        auto& s = ret.back();
        glm::vec2 bbmax;
        glm::vec2 bbmin(FLT_MAX);
        for (auto& curve : item["curves"])
        {
            auto& c = s.NewCurve();
            for (auto& point : curve["points"])
            {
                double x = point["x"].GetDouble();
                double y = 1 - point["y"].GetDouble();
                c.AddPoint(x, y);
                bbmin = glm::vec2(glm::min<float>(x, bbmin.x), glm::min<float>(y, bbmin.y));
                bbmax = glm::vec2(glm::max<float>(x, bbmax.x), glm::max<float>(y, bbmax.y));
            }
        }

        glm::vec2 fact = 0.9f / (bbmax - bbmin);
        for (auto& curve : s.curves)
            for (auto& point : curve.points)
                point.p = (point.p - bbmin) * fact + fact * 0.05f;
        //s.CalcColors2();
    }
    return ret;
}

std::vector<cv::Mat> RenderSample(const std::vector<Sample> &samples, int w, int h)
{
    std::vector<cv::Mat> mats;
    RenderTexture rtt;
    rtt.create(w, h);
    rtt.bindFramebuffer();
    for (auto& sample : samples)
    {
        rtt.RenderSample(sample);
        auto data = rtt.GetTextureData();
        mats.emplace_back(h, w, CV_32FC1, data);
    }
    rtt.unbindFramebuffer();
    rtt.destroy();
    return mats;
}

cv::PCA GeneratePCA(const cv::Mat& data, int num_components)
{
    static const char *CACHE_PCA = "cache/pca.yaml";

    cv::PCA pca;
    cv::FileStorage pca_cache;
    if (!IGNORE_CACHE && pca_cache.open(CACHE_PCA, cv::FileStorage::READ))
    {
        printf("PCA loaded from cache\n");
        pca_cache["mean"] >> pca.mean;
        pca_cache["e-vec"] >> pca.eigenvectors;
        pca_cache["e-val"] >> pca.eigenvalues;
        pca_cache.release();
    }
    else
    {
        // Perform a PCA
        printf("computing PCA for %d components...", num_components);
        pca = cv::PCA(data, cv::Mat(), CV_PCA_DATA_AS_COL, num_components);
        printf("done\n");

        // Save cache
        printf("save pca cache...");
        if (pca_cache.open(CACHE_PCA, cv::FileStorage::WRITE))
        {
            pca_cache << "mean" << pca.mean;
            pca_cache << "e-vec" << pca.eigenvectors;
            pca_cache << "e-val" << pca.eigenvalues;
            pca_cache.release();
        }
        printf("done\n");
    }
    return pca;
}

cv::Mat ProjectData(const cv::PCA& pca, const cv::Mat& data)
{
    cv::Mat pca_data(pca.eigenvectors.rows, data.cols, data.type());
    for (int i = 0; i < data.cols; i++)
    {
        auto proj = pca.project(data.col(i));
        proj.copyTo(pca_data.col(i)); // TODO: try the other overload
    }
    return pca_data;
}

std::tuple<cv::Mat, cv::Mat, cv::Mat, cv::Mat>
    GenerateData(int n_comp, float train_ratio)
{
    static const char *CACHE_DATA = "cache/data.yaml";
    static char path[256];
    static char key[32];

    int nSamples = INT_MAX;
    int nSets = 0;

    // Generate or load all the samples
    std::map<char, std::vector<cv::Mat>> mats_map;
    cv::FileStorage mats_cache;
    if (!IGNORE_CACHE && mats_cache.open(CACHE_DATA, cv::FileStorage::READ))
    {
        printf("loading samples from cache...");
        for (char c = FIRST; c <= LAST; c++)
        {
            sprintf(key, "left-%c", c);
            mats_cache[key] >> mats_map[c];
            nSets++;
        }
        printf("done\n");
        mats_cache.release();
    }
    else
    {
        mats_cache.open(CACHE_DATA, cv::FileStorage::WRITE);
        for (char c = FIRST; c <= LAST; c++)
        {
            sprintf(path, "data/left/%c.json", c);
            printf("loading %s...", path);

            // Read the samples from disk
            auto samples = ReadSamples(path);

            // Render the samples with OpenGL
            mats_map[c] = RenderSample(samples, W, H);
            printf("done with %d samples\n", (int)samples.size());

            // Get the minimum samples set,
            // in case they are not the same size
            nSamples = std::min<int>(nSamples, (int)samples.size());

            // Store the sample set in the cache
            sprintf(key, "left-%c", c);
            mats_cache << key << mats_map[c];

            nSets++;
        }
        mats_cache.release();
    }

    // Calculate the sizes based on the ratio
    // They must sum to the total number of samples
    int train_size = (int)(nSamples * train_ratio);
    int test_size = nSamples - train_size;
    assert(train_size + test_size == nSamples);

    // Convert each image sample of WxH into a vector of D dimensions
    // then stores each vector image in each column of data matrix.
    // Tags is a row vector that contains the tag for each column 
    // associated with the vector image.
    cv::Mat train_data(W*H, nSets * train_size, CV_32FC1);
    cv::Mat train_tags(nSets * train_size, 1, CV_8UC1);
    cv::Mat test_data(W*H, nSets * test_size, CV_32FC1);
    cv::Mat test_tags(nSets * test_size, 1, CV_8UC1);
    int ntrain = 0, ntest = 0; // Column index counters
    for (const auto& mp : mats_map)
    {
        int i = 0;
        for (; i < train_size; i++)
        {
            mp.second[i].reshape(1, W*H).copyTo(train_data.col(ntrain));
            train_tags.row(ntrain) = mp.first;
            ntrain++;
        }
        for (; i < nSamples; i++)
        {
            mp.second[i].reshape(1, W*H).copyTo(test_data.col(ntest));
            test_tags.row(ntest) = mp.first;
            ntest++;
        }
    }
    assert(train_data.cols + test_data.cols == nSets*nSamples);

    // Load or generate PCA projector
    cv::PCA pca = GeneratePCA(train_data, n_comp);

    // Return the projected data into the n_comp dimensions space
    return std::make_tuple(
        ProjectData(pca, train_data),   // Training projected data
        train_tags,                     // Training tags
        ProjectData(pca, test_data),    // Test projected data
        test_tags);                     // Test tags
}

std::tuple<cv::Mat, cv::Mat, cv::Mat, cv::Mat>
    SplitProjectData(const cv::Mat& data, const cv::Mat& tags, int stride,
    int n_comp = 10, float train_ratio = .9f)
{
    // Calculate the sizes based on the ratio
    // They must sum to the total number of samples
    int train_size = (int)(data.cols * train_ratio);
    int test_size = data.cols - train_size;

    assert(train_size + test_size == data.cols);

    // Split the data matrix
    cv::Mat train_data(data.rows, train_size, data.type());
    cv::Mat test_data(data.rows, test_size, data.type());

    for (int i = 0; i < train_size; i += 1)
        data.col(i * stride).setTo(train_data.col(i));

    for (int i = 0; i < train_size; i++)
        data.col(i * stride).setTo(train_data.col(i));

    assert(train_data.cols + test_data.cols == data.cols);

    // Load or generate PCA projector
    cv::PCA pca = GeneratePCA(train_data, n_comp);

    return std::make_tuple(
        ProjectData(pca, data.colRange(0, train_size)),             // Training projected data
        tags.rowRange(0, train_size),                               // Training tags
        ProjectData(pca, data.colRange(train_size, train_size + test_size)), // Test projected data
        tags.rowRange(train_size, train_size + test_size));                  // Test tags
}

int main()
{
    initOpenGL();

    // Total num of samples loaded
    int n_samples;
    // Samples per char
    int n_stride;
    
    // Images are in columns, each column is a vector of D = W x H
    // Tags is a row vector, each column contains the 
    // corresponding vector-image tag
    // Split training and test set
    cv::Mat train_data, train_tags, test_data, test_tags;
    std::tie(train_data, train_tags, test_data, test_tags) = GenerateData(10, .8f);
    
   

    FILE* fp = fopen("out.txt", "w");
    printf("char\tk=1\tk=3\tk=5\tk=10\tk=15\n");
    fprintf(fp, "char\tk=1\tk=3\tk=5\tk=10\tk=15\n");
    for (char c = FIRST; c <= LAST; c++)
    {
        printf("%c\t", c);
        fprintf(fp, "%c\t", c);
        for (auto x : { 1, 3, 5, 10, 15 })
        {
            int correct = 0;
            int wrong = 0;
            for (int t = 0; t < test_data.cols; t++)
            {
                cv::Mat test = test_data.col(t);
            
                double minf = DBL_MAX;
                char minc = '\0';
                std::vector<std::pair<char, double>> diffs;
            
                for (int i = 0; i < train_data.cols; i++)
                {
                    cv::Mat train = train_data.col(i);
                    double diff = norm(test - train);
                    char tag = train_tags.at<char>(i, 0);
                    diffs.emplace_back(tag, diff);
                }

                int K = x;
                std::sort(diffs.begin(), diffs.end(),
                    [](const std::pair<char, double>& a, const std::pair<char, double>& b) { return a.second < b.second; });
                std::map<char, int> counter;
                int max_count = -1;
                for (int i = 0; i <= K; i++)
                {
                    if (counter.count(diffs[i].first))
                        counter[diffs[i].first]++;
                    else
                        counter[diffs[i].first] = 0;
                    if (counter[diffs[i].first] > max_count)
                    {
                        max_count = counter[diffs[i].first];
                        minc = diffs[i].first;
                    }
                }

                if (c != minc)
                    wrong++;
                else
                    correct++;
            }
            printf("%3.2f\t", (float)correct / (wrong + correct) * 100.f);
            fprintf(fp, "%3.2f\t", (float)correct / (wrong + correct) * 100.f);
        }
        printf("\n");
        fprintf(fp, "\n");
    }
    fclose(fp);

    //auto KNN = ml::KNearest::create();
    //auto trainData = ml::TrainData::create(pca_data, ml::COL_SAMPLE)
    //    KNN->train(

    UnloadOpenGL();
    printf("bye\n");
    system("pause");
    return 0;
}
