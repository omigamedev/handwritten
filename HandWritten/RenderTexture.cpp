#include "stdafx.h"
#include "RenderTexture.h"
#include <stb.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include "Sample.h"

RenderTexture::RenderTexture()
{
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

RenderTexture::~RenderTexture()
{
    destroy();
}

void RenderTexture::destroy()
{
    if (rboID)
    {
        glDeleteRenderbuffers(1, &rboID);
    }
    if (texID)
    {
        unbindTexture();
        glDeleteTextures(1, &texID);
    }
    if (fboID)
    {
        unbindFramebuffer();
        glDeleteFramebuffers(1, &fboID);
    }
    fboID = 0;
    rboID = 0;
    w = 0;
    h = 0;
}

bool RenderTexture::create(int width, int height)
{
    // Destroy any previously created object
    destroy();

    w = width;
    h = height;

    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create a renderbuffer object to store depth info
    glGenRenderbuffers(1, &rboID);
    glBindRenderbuffer(GL_RENDERBUFFER, rboID);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    // Create a framebuffer object
    glGenFramebuffers(1, &fboID);
    glBindFramebuffer(GL_FRAMEBUFFER, fboID);

    // Attach the texture to FBO color attachment point
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texID, 0);

    // Attach the renderbuffer to depth attachment point
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboID);

    // Check FBO status
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
        printf("createColorBuffer failed\n");

    // Switch back to window-system-provided framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return status == GL_FRAMEBUFFER_COMPLETE;
}

void RenderTexture::bindFramebuffer() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, fboID);
}

void RenderTexture::unbindFramebuffer() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void RenderTexture::clear(glm::vec4 color) const
{
    glClearColor(color.r, color.g, color.b, color.a);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderTexture::bindTexture() const
{
    glBindTexture(GL_TEXTURE_2D, texID);
}

void RenderTexture::unbindTexture() const
{
    glBindTexture(GL_TEXTURE_2D, 0);
}

bool RenderTexture::WritePNG(const std::string filename) const
{
    auto buffer = new std::uint8_t[w*h * 4];
    bindTexture();
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    bool ret = stbi_write_png(filename.c_str(), w, h, 4, buffer, w * 4) == 1;
    delete buffer;
    return ret;
}

float* RenderTexture::GetTextureData() const
{
    auto buffer = new float[w*h];
    bindTexture();
    glGetTexImage(GL_TEXTURE_2D, 0, GL_LUMINANCE, GL_FLOAT, buffer);
    return buffer;
}

void RenderTexture::RenderSample(const Sample& sample) const
{
    glViewport(0, 0, w, h);
    glClearColor(1, 1, 1, 1);
    glLoadIdentity();
    glScalef(1, -1, 1);
    glTranslatef(0, -1, 0);

    glClear(GL_COLOR_BUFFER_BIT);
    for (auto& curve : sample.curves)
    {
        glBegin(GL_LINE_STRIP);
        for (auto& point : curve.points)
        {
            glColor3f(point.c.r, point.c.g, point.c.b);
            glVertex3f(point.p.x, point.p.y, 0);
        }
        glEnd();
    }
    glFinish();
}
