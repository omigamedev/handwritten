#pragma once

#if _WIN32
#include "targetver.h"
#endif 

#include <map>
#include <cstdio>
#include <memory>
#include <vector>
#include <array>
#include <cassert>
#include <numeric>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/common.hpp>
#include <glm/gtx/compatibility.hpp>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/filestream.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core_c.h>

namespace rapidjson 
{
    template <typename Encoding, typename Allocator>
    typename GenericValue<Encoding, Allocator>::ValueIterator begin(GenericValue<Encoding, Allocator>& v) { return v.Begin(); }
    template <typename Encoding, typename Allocator>
    typename GenericValue<Encoding, Allocator>::ConstValueIterator begin(const GenericValue<Encoding, Allocator>& v) { return v.Begin(); }

    template <typename Encoding, typename Allocator>
    typename GenericValue<Encoding, Allocator>::ValueIterator end(GenericValue<Encoding, Allocator>& v) { return v.End(); }
    template <typename Encoding, typename Allocator>
    typename GenericValue<Encoding, Allocator>::ConstValueIterator end(const GenericValue<Encoding, Allocator>& v) { return v.End(); }
} // namespace rapidjson
