#pragma once

namespace rj = rapidjson;

struct Point
{
    // Position
    glm::vec2 p;
    // Color RGB
    glm::vec3 c;
    // Distance from previous point
    float d{ .0f };
    // Normalized location relative to the path
    float t{ .0f };
    Point(float x, float y) : p(x, y) { }
};

struct Curve
{
    std::vector<Point> points;
    // Total path length
    float length{ 0 };
    void AddPoint(float x, float y);

    //************************************
    // Method:    Calculate points relative position
    // Parameter: float t0 Starting value for t, used for multiple curves
    // Parameter: float length Total length for all curves
    //************************************
    void CalcRelativePositions(float t0, float length);
    void CalcRelativePositions();
};

struct Sample
{
    std::vector<Curve> curves;
    char tag;
    float* data;
    cv::Mat mat;
    cv::Mat vec;
    
    Curve& NewCurve();
    float CalcLength() const;
    // Per curve gradient
    void CalcColors1();
    // Per sample gradient
    void CalcColors2();
};
