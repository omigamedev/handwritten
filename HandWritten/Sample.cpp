#include "stdafx.h"
#include "Sample.h"

void Curve::CalcRelativePositions()
{
    CalcRelativePositions(0.f, length);
}

void Curve::CalcRelativePositions(float t0, float length)
{
    float tsum = t0;
    float inv_length = 1.0f / length;
    for (auto& point : points)
    {
        tsum += point.d;
        point.t = tsum * inv_length;
    }
}

void Curve::AddPoint(float x, float y)
{
    Point p{ x, y };

    // Incrementally update the path length
    if (points.size())
    {
        p.d = glm::length(points.back().p - p.p);
        length += p.d;
    }

    points.push_back(p);
}

void Sample::CalcColors2()
{
    glm::vec3 c0{ 0.0f };
    glm::vec3 c1{ 1.0f };
    float length = CalcLength();
    float t0 = .0f;
    for (auto& curve : curves)
    {
        curve.CalcRelativePositions(t0, length);
        for (auto& point : curve.points)
        {
            point.c = glm::lerp(c0, c1, point.t);
        }
        t0 += curve.length;
    }
}

void Sample::CalcColors1()
{
    glm::vec3 c0{ 0.0f };
    glm::vec3 c1{ 1.0f };
    for (auto& curve : curves)
    {
        curve.CalcRelativePositions();
        for (auto& point : curve.points)
        {
            point.c = glm::lerp(c0, c1, point.t);
        }
    }
}

float Sample::CalcLength() const
{
    return std::accumulate(curves.cbegin(), curves.cend(), 0.f,
        [](float a, const Curve& b){ return a + b.length; });
}

Curve& Sample::NewCurve()
{
    curves.emplace_back();
    return curves.back();
}
