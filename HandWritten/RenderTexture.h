#pragma once

struct Sample;

class RenderTexture
{
    GLuint fboID;
    GLuint rboID;
    GLuint texID;
    int w;
    int h;

public:
    RenderTexture();
    ~RenderTexture();

    void destroy();
    bool create(int width, int height);
    void clear(glm::vec4 color = glm::vec4(0)) const;
    void bindFramebuffer() const;
    void unbindFramebuffer() const;
    void bindTexture() const;
    void unbindTexture() const;
    GLuint getTextureID() const { return texID; }
    int getWidth() const { return w; }
    int getHeight() const { return h; }
    void RenderSample(const Sample& sample) const;
    float* GetTextureData() const;
    bool WritePNG(const std::string filename) const;
};

