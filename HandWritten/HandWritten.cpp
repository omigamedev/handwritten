#include "stdafx.h"
#include "RenderTexture.h"
#include "Sample.h"

#define ml cv::ml
GLFWwindow* window;
int W = 16;
int H = 16;
int D = W * H;

#ifdef _DEBUG
    #define LOG printf
#else
    #define LOG
#endif

std::vector<char> GenerateCharset(char from, char to)
{
    std::vector<char> ret;
    for (char c = from; c <= to; c++)
        ret.push_back(c);
    return ret;
}

std::vector<Sample> ReadSamples(const char* filename)
{
    std::vector<Sample> ret;
    rj::Document doc;
    rj::FileStream fs(std::fopen(filename, "r"));
    doc.ParseStream(fs);
    for (auto& item : doc)
    {
        ret.emplace_back();
        auto& s = ret.back();
        glm::vec2 bbmax;
        glm::vec2 bbmin(FLT_MAX);
        for (auto& curve : item["curves"])
        {
            auto& c = s.NewCurve();
            for (auto& point : curve["points"])
            {
                double x = point["x"].GetDouble();
                double y = 1 - point["y"].GetDouble();
                c.AddPoint(x, y);
                bbmin = glm::vec2(glm::min<float>(x, bbmin.x), glm::min<float>(y, bbmin.y));
                bbmax = glm::vec2(glm::max<float>(x, bbmax.x), glm::max<float>(y, bbmax.y));
            }
        }

        glm::vec2 fact = 0.9f / (bbmax - bbmin);
        for (auto& curve : s.curves)
            for (auto& point : curve.points)
                point.p = (point.p - bbmin) * fact + fact * 0.05f;
        //s.CalcColors2();
    }
    return ret;
}

void RenderSample(std::vector<Sample>& samples, int w, int h)
{
    std::vector<cv::Mat> mats;
    RenderTexture rtt;
    rtt.create(w, h);
    rtt.bindFramebuffer();
    for (auto& sample : samples)
    {
        rtt.RenderSample(sample);
        sample.data = rtt.GetTextureData();
        sample.mat = cv::Mat(h, w, CV_32FC1, sample.data);
    }
    rtt.unbindFramebuffer();
    rtt.destroy();
}

std::tuple<
    std::map<char, std::map<int/*K*/, std::pair<int/*tests*/, int/*success*/>>>, // results
    std::map<int, float> // mean
> DoNN(const std::vector<Sample>& test_set, const std::vector<Sample>& train_set)
{
    std::map<char, std::map<int, std::pair<int, int>>> results;
    for (auto K : { 1, 3, 5, 10, 15 })
    {
        int test_id = 0;
        for (const auto& test : test_set)
        {
            LOG("K=%d Testing %c %04d/%d\r", K, test.tag, test_id++, (int)test_set.size());
            std::vector<std::pair<char, double>> diffs;

            // Test with each element of training set
            std::pair<int, float> diffs_limit(-1, 0.f);
            for (const auto& train : train_set)
            {
                double diff = cv::norm(test.vec - train.vec);
                diffs.emplace_back(train.tag, diff);
            }

            std::sort(diffs.begin(), diffs.end(),
                [](const std::pair<char, double>& a, const std::pair<char, double>& b) { return a.second < b.second; });
            std::map<char, int> counter;
            int max_count = -1;
            char min_tag = '\0';
            for (int i = 0; i <= K && i < diffs.size(); i++)
            {
                if (counter.count(diffs[i].first))
                    counter[diffs[i].first]++;
                else
                    counter[diffs[i].first] = 0;
                if (counter[diffs[i].first] > max_count)
                {
                    max_count = counter[diffs[i].first];
                    min_tag = diffs[i].first;
                }
            }

            if (test.tag == min_tag)
                results[test.tag][K].second++;
            results[test.tag][K].first++;
        }
        LOG("\n");
    }

    std::map<int, float> means;
    for (const auto& char_group : results)
        for (const auto& r : char_group.second)
            means[r.first] += (float)r.second.second / (float)r.second.first;
    for (auto& mean : means)
        mean.second = (float)mean.second / (float)results.size() * 100.f;

    return std::make_tuple(results, means);
}

void init()
{
    LOG("init\n");
    glfwInit();
    glfwWindowHint(GLFW_VISIBLE, false);
    window = glfwCreateWindow(800, 600, "Reader", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, [](GLFWwindow* w, int key, int, int, int)
    {
        if (key == GLFW_KEY_ESCAPE)
            glfwSetWindowShouldClose(w, 1);
    });

    glewInit();
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_DST_COLOR, GL_ZERO);
    glPointSize(15);
    glLineWidth(3);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1, 0, 1, -1, 1);
    glMatrixMode(GL_MODELVIEW);
}

void uninit()
{
    glfwDestroyWindow(window);
    glfwTerminate();
    printf("bye\n");
    system("pause");
}

std::tuple<int, std::map<char, std::vector<Sample>>> LoadData(const std::vector<char>& charset)
{
    int samples_per_char = INT_MAX;
    cv::FileStorage mats_cache;
    std::map<char, std::vector<Sample>> samples_map;
    printf("Loading: ");
    for (auto c : charset)
    {
        static char path[256];
        sprintf(path, "data/left/%c.json", c);
        samples_map[c] = ReadSamples(path);
        RenderSample(samples_map[c], W, H);
        for (auto& sample : samples_map[c])
            sample.tag = c;
        samples_per_char = std::min(samples_per_char, (int)samples_map[c].size());
        printf("%c", c);
    }
    printf("\nLoaded: %d chars per %d samples\n", (int)samples_map.size(), samples_per_char);
    return make_tuple(samples_per_char, samples_map);
}

std::tuple<std::vector<Sample>, std::vector<Sample>>
SplitData(const std::map<char, std::vector<Sample>>& samples_map, int samples_per_char, float train_ratio)
{
    LOG("Split: train=%.2f%%(%d) test=%d\n", train_ratio,
        (int)(train_ratio * samples_per_char),
        (int)((1.f - train_ratio) * samples_per_char));
    // Split the data
    std::vector<Sample> train_set;
    std::vector<Sample> test_set;
    for (const auto& map : samples_map)
    {
        int i = 0;
        for (; i < samples_per_char * train_ratio; i++)
            train_set.push_back(map.second[i]);
        for (; i < samples_per_char; i++)
            test_set.push_back(map.second[i]);
    }
    return std::make_tuple(train_set, test_set);
}

void CompressDataPCA(std::vector<Sample>& train_set, std::vector<Sample>& test_set, int n_comp)
{
    // Create images matrix for PCA
    LOG("Compute PCA\n");
    cv::Mat data(D, (int)train_set.size(), CV_32FC1);
    for (int i = 0; i < train_set.size(); i++)
        cv::Mat(D, 1, CV_32FC1, train_set[i].data).copyTo(data.col(i));
    // Project all with PCA
    LOG("Project samples\n");
    cv::PCA pca(data, cv::Mat(), CV_PCA_DATA_AS_COL, n_comp);
    for (auto& sample : train_set)
        pca.project(sample.mat.reshape(1, D)).copyTo(sample.vec);
    for (auto& sample : test_set)
        pca.project(sample.mat.reshape(1, D)).copyTo(sample.vec);
}

int main()
{
    init();
    glLineWidth(2);

    int N_COMP = 10;

    auto chars = GenerateCharset('A', 'Z');

    auto data = LoadData(chars);
    auto samples_per_char = std::get<0>(data);
    auto samples_map = std::get<1>(data);
    
    // Force to only use x samples per each char
    samples_per_char = 50;

    printf("\nRatio\tk=1\tk=3\tk=5\tk=10\tk=15\n");
    for (float train_ratio : {.01, .05, .1, .2})
    {
        auto split = SplitData(samples_map, samples_per_char, train_ratio);
        auto train_set = std::get<0>(split);
        auto test_set = std::get<1>(split);
        CompressDataPCA(train_set, test_set, N_COMP);

        auto nndata = DoNN(train_set, test_set);
        auto results = std::get<0>(nndata);
        auto means = std::get<1>(nndata);

        // Print the results for this K
        //printf("\nchar\tk=1\tk=3\tk=5\tk=10\tk=15\n");
        //for (const auto& char_group : results)
        //{
        //    printf("%c\t", char_group.first);
        //    for (const auto& r : char_group.second)
        //        printf("%3.2f\t", (float)r.second.second / (float)r.second.first * 100.f);
        //    printf("\n");
        //}

        // Print means
        printf("%.3f%%\t", train_ratio);
        for (const auto& mean : means)
            printf("%3.2f\t", mean.second);
        printf("\t train(%d) test(%d)\n", (int)train_set.size(), (int)test_set.size());
    }
    
    uninit();
    return 0;
}
